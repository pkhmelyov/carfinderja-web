﻿using CarfinderjaWeb.Data;
using CarfinderjaWeb.Data.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;

namespace CarfinderjaWeb.Infrastructure
{
    public class AppUserManager : UserManager<AppUser>
    {
        public AppUserManager(IUserStore<AppUser> store) : base(store) { }

        public static AppUserManager Create(IdentityFactoryOptions<AppUserManager> options, IOwinContext context)
        {
            var db = context.Get<CarfinderjaDataContext>();
            var manager = new AppUserManager(new UserStore<AppUser>(db));
            return manager;
        }
    }
}