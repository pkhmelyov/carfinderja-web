﻿using CarfinderjaWeb.Data.Entities;
using CarfinderjaWeb.ViewModels.Home;

namespace CarfinderjaWeb.Services
{
    public interface INotificationService
    {
        void SendContactUsNotification(ContactUs model);
        void SendNewCommentNotification(PostComment comment);
    }
}