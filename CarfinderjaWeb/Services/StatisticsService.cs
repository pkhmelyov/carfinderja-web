﻿using CarfinderjaWeb.Data;
using CarfinderjaWeb.Data.Repositories;
using Newtonsoft.Json;

namespace CarfinderjaWeb.Services
{
    public class StatisticsService : IStatisticsService
    {
        private readonly ISiteStatisticsRepository _siteStatisticsRepository;
        public StatisticsService(ISiteStatisticsRepository siteStatisticsRepository)
        {
            _siteStatisticsRepository = siteStatisticsRepository;
        }

        public Statistics GetStatistics()
        {
            var stats = _siteStatisticsRepository.GetStatistics();
            if (stats == null || string.IsNullOrEmpty(stats.Data)) return null;
            return JsonConvert.DeserializeObject<Statistics>(stats.Data);
        }
    }
}