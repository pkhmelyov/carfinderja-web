﻿using System;
using CarfinderjaWeb.Data.Entities;
using CarfinderjaWeb.ViewModels.Home;
using RestSharp;
using RestSharp.Authenticators;

namespace CarfinderjaWeb.Services
{
    public class EmailMailGunAPINotificationService : INotificationService
    {
        private readonly string _contactUsFrom;
        private readonly string[] _contactUsTo;
        private readonly string _apiKey;

        public EmailMailGunAPINotificationService(string contactUsFrom, string[] contactUsTo, string apiKey)
        {
            _contactUsFrom = contactUsFrom;
            _contactUsTo = contactUsTo;
            _apiKey = apiKey;
        }

        public void SendContactUsNotification(ContactUs model)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", _apiKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "mg.carfinderja.com", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", string.Format("Carfinderja <{0}>", _contactUsFrom));
            foreach (var item in _contactUsTo)
            {
                request.AddParameter("to", item);
            }
            request.AddParameter("subject", "[Carfinderja Contact Us]");
            request.AddParameter("text",
                string.Format("Name: {0}\nCountry code: {1}\nPhone number: {2}\nEmail: {3}\nMessage: {4}", model.Name,
                    model.CountryCode, model.PhoneNumber, model.Email, model.Message));
            request.Method = Method.POST;
            var response = client.Execute(request);
        }

        public void SendNewCommentNotification(PostComment comment)
        {

            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", _apiKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "mg.carfinderja.com", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", string.Format("Carfinderja <{0}>", _contactUsFrom));
            foreach (var item in _contactUsTo)
            {
                request.AddParameter("to", item);
            }
            request.AddParameter("subject", "[Carfinderja New Comment]");
            request.AddParameter("html",
                string.Format(
                @"
Blog Post: <a href=""http://carfinderja.com/Blog/Post/{0}"">{1}</a><br />
Name: {2}<br />
Email: {3}<br />
Comment: {4}<br />
<a href=""http://carfinderja.com/Admin/Dashboard/Comment/{5}"">Approve or delete this comment</a>", comment.BlogPostId, comment.Post.Title, comment.Name, comment.Email, comment.Comment, comment.Id));
            request.Method = Method.POST;
            client.Execute(request);
        }
    }
}