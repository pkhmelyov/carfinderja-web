﻿using System.Net.Mail;
using CarfinderjaWeb.Data.Entities;
using CarfinderjaWeb.ViewModels.Home;

namespace CarfinderjaWeb.Services
{
    public class EmailNotificationService : INotificationService
    {
        private readonly string _contactUsFrom;
        private readonly string[] _contactUsTo;

        public EmailNotificationService(string contactUsFrom, string[] contactUsTo)
        {
            _contactUsFrom = contactUsFrom;
            _contactUsTo = contactUsTo;
        }

        public void SendContactUsNotification(ContactUs model)
        {
            var smtpClient = new SmtpClient();
            var message = new MailMessage();
            message.Subject = "[Carfinderja Contact Us]";
            message.Body = string.Format("Name: {0}\nCountry code: {1}\nPhone number: {2}\nEmail: {3}\nMessage: {4}", model.Name, model.CountryCode, model.PhoneNumber, model.Email, model.Message);
            message.From = new MailAddress(_contactUsFrom, "Carfinderja");
            foreach (var item in _contactUsTo)
            {
                message.To.Add(new MailAddress(item));
                smtpClient.Send(message);
                message.To.Clear();
            }
        }

        public void SendNewCommentNotification(PostComment comment)
        {
            var smtpClient = new SmtpClient();
            var message = new MailMessage() {IsBodyHtml = true};
            message.Subject = "[Carfinderja New Comment]";
            message.Body = string.Format(
                @"
Blog Post: <a href=""http://carfinderja.local/Blog/Post/{0}"">{1}</a><br />
Name: {2}<br />
Email: {3}<br />
Comment: {4}<br />
<a href=""http://carfinderja.local/Admin/Dashboard/Comment/{5}"">Approve or delete this comment</a>", comment.BlogPostId, comment.Post.Title, comment.Name, comment.Email, comment.Comment, comment.Id);
            message.From = new MailAddress(_contactUsFrom, "Carfinderja");
            foreach (var item in _contactUsTo)
            {
                message.To.Add(new MailAddress(item));
                smtpClient.Send(message);
                message.To.Clear();
            }
        }
    }
}