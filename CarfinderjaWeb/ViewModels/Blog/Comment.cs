﻿using System.ComponentModel.DataAnnotations;

namespace CarfinderjaWeb.ViewModels.Blog
{
    public class Comment
    {
        public int BlogPostId { get; set; }
        [Required]
        [StringLength(25)]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [StringLength(250)]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
    }
}