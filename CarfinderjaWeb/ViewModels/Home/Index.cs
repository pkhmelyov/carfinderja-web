﻿using System.ComponentModel;
using System.Web.Mvc;
using PagedList;
using CarfinderjaWeb.Data.Entities;
using CarfinderjaWeb.Data;

namespace CarfinderjaWeb.ViewModels.Home {
	public class Index {
		public IPagedList<Advertisement> Items { get; set; }
		public Make[] Makes { get; set; }
		public Model[] Models { get; set; }
		public string[] Transmissions { get; set; }
		public int Page { get; set; }
		public int? MakeId { get; set; }
		public int? ModelId { get; set; }
		public SelectListItem[] SortByItems { get; set; }
		public SelectListItem[] PriceRangeItems { get; set; }

		[DisplayName("Type")]
		public Transmission? Transmission { get; set; }

		[DisplayName("Year from")]
		public int? YearFrom { get; set; }

		[DisplayName("Year to")]
		public int? YearTo { get; set; }

		[DisplayName("Price range JMD")]
		public PriceRange? PriceRange { get; set; }

		[DisplayName("Sort By")]
		public int SortBy { get; set; }
	}
}