﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CarfinderjaWeb.ViewModels.Home
{
    public class ContactUs
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your name")]
        [StringLength(50, ErrorMessage = "Name cannot exceed 50 characters.")]
        [DisplayName("Name")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter country code")]
        [StringLength(50, ErrorMessage = "Country code cannot exceed 50 characters.")]
        [DisplayName("Country code")]
        public string CountryCode { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter Phone number")]
        [DisplayName("Phone number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Invalid Phone number")]
        public string PhoneNumber { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your email")]
        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$", ErrorMessage = "You must provide a valid email address.")]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please enter your message")]
        [DisplayName("Message")]
        public string Message { get; set; }
    }
}