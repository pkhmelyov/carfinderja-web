﻿using System;
using System.Web.Mvc;
using CarfinderjaWeb.Data.Entities;
using CarfinderjaWeb.Data.Repositories;
using CarfinderjaWeb.Services;
using CarfinderjaWeb.ViewModels.Blog;

namespace CarfinderjaWeb.Controllers
{
    public class BlogController : Controller
    {
        private readonly IBlogPostRepository _blogPostRepository;
        private readonly INotificationService _notificationService;

        public BlogController(IBlogPostRepository blogPostRepository, INotificationService notificationService)
        {
            _blogPostRepository = blogPostRepository;
            _notificationService = notificationService;
        }

        public ActionResult Index(int page = 1)
        {
            var model = _blogPostRepository.GetRecentActivePosts(pageNumber: page);
            return View(model);
        }

        public ActionResult Post(int id)
        {
            var post = _blogPostRepository.GetById(id);
            if (post == null || !post.Active) return HttpNotFound("There is no such Blog Post");
            return View(post);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Comment(Comment model)
        {
            if (ModelState.IsValid)
            {
                var post = _blogPostRepository.GetById(model.BlogPostId);
                if (post == null || !post.Active) return HttpNotFound("There is no such Blog Post");
                var comment = new PostComment
                {
                    Name = model.Name,
                    Email = model.Email,
                    Comment = model.Message,
                    CreatedOn = DateTime.UtcNow,
                    Active = false
                };
                post.Comments.Add(comment);
                _blogPostRepository.SaveChanges();
                _notificationService.SendNewCommentNotification(comment);
            }
            return RedirectToAction("Post", "Blog", new {id = model.BlogPostId});
        }
    }
}