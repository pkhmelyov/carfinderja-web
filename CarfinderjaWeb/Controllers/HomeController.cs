﻿using System;
using System.Linq;
using System.Web.Mvc;
using CarfinderjaWeb.ViewModels.Home;
using CarfinderjaWeb.Data;
using CarfinderjaWeb.Data.Repositories;
using CarfinderjaWeb.Services;

namespace CarfinderjaWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IAdvertisementRepository _advertismentRepository;
        private readonly IMakeRepository _makeRepository;
        private readonly INotificationService _notificationService;
        private readonly IStatisticsService _statisticsService;
        private const int PageSize = 16;

        public HomeController(IAdvertisementRepository advertisementRepository, IMakeRepository makeRepository, INotificationService notificationService, IStatisticsService statisticsService)
        {
            _advertismentRepository = advertisementRepository;
            _makeRepository = makeRepository;
            _notificationService = notificationService;
            _statisticsService = statisticsService;
        }

        public ActionResult Index(int page = 1, int? makeId = null, int? modelId = null, Transmission? transmission = null, int? yearFrom = null, int? yearTo = null, PriceRange? priceRange = null, SortBy sortBy = SortBy.DateAddedDesc)
        {
            if (yearFrom != null && yearTo != null && yearFrom > yearTo)
            {
                var temp = yearFrom;
                yearFrom = yearTo;
                yearTo = temp;
            }

            var viewModel = new Index
            {
                Page = page,
                MakeId = makeId,
                ModelId = modelId,
                Transmission = transmission,
                YearFrom = yearFrom,
                YearTo = yearTo,
                PriceRange = priceRange,
                SortBy = (int)sortBy,
                Makes = _makeRepository.GetMakes(),
                Models = _makeRepository.GetModels(makeId),
                SortByItems = GetSortByItems(sortBy),
                PriceRangeItems = GetPriceRangeItems(priceRange),
                Transmissions = new[] { "Automatic", "CVT", "Manual", "Tiptronic", "Column" },
                Items = _advertismentRepository.FillModel(makeId, modelId, transmission, yearFrom, yearTo, priceRange, sortBy, page, PageSize),
            };
            return View("IndexAutoShowWeb", viewModel);
        }

        public JsonResult GetModels(int? makeId)
        {
            return Json(_makeRepository.GetModels(makeId).Select(model => new { model.Id, model.Name }).OrderBy(model => model.Name).ToArray(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult ContactUs()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ContactUs(ContactUs model)
        {
            if (ModelState.IsValid)
            {
                var text = model.Message;
                var email = model.Email;
                _notificationService.SendContactUsNotification(model);
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Statistics()
        {
            var model = _statisticsService.GetStatistics();
            return View(model);
        }

        private SelectListItem[] GetSortByItems(SortBy sortBy)
        {
            return new[] {
                new SelectListItem {Text = "Sort by Price - Low to High", Value = ((int)SortBy.PriceAsc).ToString(), Selected = sortBy == SortBy.PriceAsc},
                new SelectListItem {Text = "Sort by Price - High to Low", Value = ((int)SortBy.PriceDesc).ToString(), Selected = sortBy == SortBy.PriceDesc},
                new SelectListItem {Text = "Sort by Year - Old to New", Value = ((int)SortBy.YearAsc).ToString(), Selected = sortBy == SortBy.YearAsc},
                new SelectListItem {Text = "Sort by Year - New to Old", Value = ((int)SortBy.YearDesc).ToString(), Selected = sortBy == SortBy.YearDesc},
                new SelectListItem {Text = "Sort by Date added - Old to New", Value = ((int)SortBy.DateAddedAsc).ToString(), Selected = sortBy == SortBy.DateAddedAsc},
                new SelectListItem {Text = "Sort by Date added - New to Old", Value = ((int)SortBy.DateAddedDesc).ToString(), Selected = sortBy == SortBy.DateAddedDesc},
            };
        }

        private SelectListItem[] GetPriceRangeItems(PriceRange? priceRange)
        {
            return new[] {
                new SelectListItem {
                    Text = "0 - 500,000",
                    Value = Enum.GetName(typeof(PriceRange), PriceRange.I_0_500000),
                    Selected = priceRange == PriceRange.I_0_500000
                },
                new SelectListItem {
                    Text = "500,000 - 1,000,000",
                    Value = Enum.GetName(typeof(PriceRange), PriceRange.I_500000_1000000),
                    Selected = priceRange == PriceRange.I_500000_1000000
                },
                new SelectListItem {
                    Text = "1,000,000 - 1,500,000",
                    Value = Enum.GetName(typeof(PriceRange), PriceRange.I_1000000_1500000),
                    Selected = priceRange == PriceRange.I_1000000_1500000
                },
                new SelectListItem {
                    Text = "1,500,000 - 2,000,000",
                    Value = Enum.GetName(typeof(PriceRange), PriceRange.I_1500000_2000000),
                    Selected = priceRange == PriceRange.I_1500000_2000000
                },
                new SelectListItem {
                    Text = "2,000,000 - 3,000,000",
                    Value = Enum.GetName(typeof(PriceRange), PriceRange.I_2000000_3000000),
                    Selected = priceRange == PriceRange.I_2000000_3000000
                },
                new SelectListItem {
                    Text = "3,000,000 - 4,000,000",
                    Value = Enum.GetName(typeof(PriceRange), PriceRange.I_3000000_4000000),
                    Selected = priceRange == PriceRange.I_3000000_4000000
                },
                new SelectListItem {
                    Text = "4,000,000 - 5,000,000",
                    Value = Enum.GetName(typeof(PriceRange), PriceRange.I_4000000_5000000),
                    Selected = priceRange == PriceRange.I_4000000_5000000
                },
                new SelectListItem {
                    Text = "> 5,000,000",
                    Value = Enum.GetName(typeof(PriceRange), PriceRange.I_GT_5000000),
                    Selected = priceRange == PriceRange.I_GT_5000000
                },
            };
        }
    }
}