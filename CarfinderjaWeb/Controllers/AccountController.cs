﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using CarfinderjaWeb.Infrastructure;
using CarfinderjaWeb.Data.Entities;

namespace CarfinderjaWeb.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private IAuthenticationManager AuthManager { get { return HttpContext.GetOwinContext().Authentication; } }

        private AppUserManager UserManager { get { return HttpContext.GetOwinContext().GetUserManager<AppUserManager>(); } }

        private string[] AllowedEmails { get { return ConfigurationManager.AppSettings["allowedEmails"].Split(','); } }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            return RedirectToAction("GoogleLogin", "Account", new {returnUrl});
        }

        [AllowAnonymous]
        public ActionResult GoogleLogin(string returnUrl)
        {
            AuthenticationProperties properties = new AuthenticationProperties
            {
                RedirectUri = Url.Action("GoogleResponse", "Account", new { returnUrl = returnUrl })
            };
            AuthManager.Challenge(properties, "Google");
            return new HttpUnauthorizedResult();
        }

        [AllowAnonymous]
        public async Task<ActionResult> GoogleResponse(string returnUrl)
        {
            ExternalLoginInfo loginInfo = await AuthManager.GetExternalLoginInfoAsync();
            AppUser user = await UserManager.FindAsync(loginInfo.Login);
            if (user == null && Array.Exists(AllowedEmails, s => s.ToLowerInvariant().Trim() == loginInfo.Email))
            {
                user = new AppUser
                {
                    Email = loginInfo.Email,
                    UserName = loginInfo.Email,
                };
                IdentityResult result = await UserManager.CreateAsync(user);
                if (!result.Succeeded)
                {
                    return View("Error", result.Errors);
                }
                else
                {
                    result = await UserManager.AddLoginAsync(user.Id, loginInfo.Login);
                    if (!result.Succeeded)
                    {
                        return View("Error", result.Errors);
                    }
                }
            }
            if (user == null)
            {
                return Redirect("/");
            }
            ClaimsIdentity ident = await UserManager.CreateIdentityAsync(user,
            DefaultAuthenticationTypes.ApplicationCookie);
            ident.AddClaims(loginInfo.ExternalIdentity.Claims);
            AuthManager.SignOut();
            AuthManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = false
            }, ident);
            return Redirect(returnUrl ?? "/");
        }

        [Authorize]
        public ActionResult Logout()
        {
            AuthManager.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}