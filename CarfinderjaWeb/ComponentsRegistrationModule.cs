﻿using Autofac;
using CarfinderjaWeb.Services;
using System.Configuration;

namespace CarfinderjaWeb
{
    public class ComponentsRegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(
                context =>
                    new EmailMailGunAPINotificationService(ConfigurationManager.AppSettings["ContactUsFrom"],
                        ConfigurationManager.AppSettings["ContactUsTo"].Split(','), ConfigurationManager.AppSettings["MailGunApiKey"]))
                .As<INotificationService>()
                .InstancePerDependency();

            builder.RegisterType<StatisticsService>().As<IStatisticsService>().InstancePerDependency();
        }
    }
}