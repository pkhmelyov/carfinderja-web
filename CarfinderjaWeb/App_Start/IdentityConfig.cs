﻿using CarfinderjaWeb.Data;
using CarfinderjaWeb.Infrastructure;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Owin;

namespace CarfinderjaWeb
{
    public class IdentityConfig
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext<CarfinderjaDataContext>(CarfinderjaDataContext.Create);
            app.CreatePerOwinContext<AppUserManager>(AppUserManager.Create);
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });

            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            var googleOptions = new GoogleOAuth2AuthenticationOptions
            {
                ClientId = "573654326223-660lju9shu4cd4vps4ldagda3gbrlq72.apps.googleusercontent.com",
                ClientSecret = "s4VLFHf51c5WAj3zCnqxZRBS"
            };
            googleOptions.Scope.Add("https://www.googleapis.com/auth/userinfo.email");
            app.UseGoogleAuthentication(googleOptions);
        }
    }
}