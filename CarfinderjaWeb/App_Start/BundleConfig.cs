﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace CarfinderjaWeb {
	public class BundleConfig {
		public static void RegisterBundles(BundleCollection bundles) {
			bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap.css", "~/Content/bootstrap.flatly-theme.min.css", "~/Content/3-col-portfolio.css", "~/Content/PagedList.css"));
			bundles.Add(new ScriptBundle("~/bundles/scripts").Include("~/Scripts/jquery-{version}.js", "~/Scripts/bootstrap.js"));

			bundles.Add(new StyleBundle("~/Content/css/AutoShowWeb/home").Include("~/Content/skins/AutoShowWeb/css/bootstrap.css", "~/Content/skins/AutoShowWeb/css/style.css", "~/Content/skins/AutoShowWeb/css/component.css"));
			bundles.Add(new ScriptBundle("~/bundles/scripts/AutoShowWeb/home").Include("~/Scripts/skins/AutoShowWeb/js/jquery-1.11.1.min.js", "~/Scripts/skins/AutoShowWeb/js/bootstrap.min.js"));
			bundles.Add(new ScriptBundle("~/bundles/scripts/AutoShowWeb/home/bottom").Include("~/Scripts/skins/AutoShowWeb/js/cbpViewModeSwitch.js", "~/Scripts/skins/AutoShowWeb/js/classie.js", "~/Scripts/contactus.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/AutoShowWeb/contactus/bottom").Include("~/Scripts/skins/AutoShowWeb/js/classie.js", "~/Scripts/contactus.js"));

            bundles.Add(new StyleBundle("~/Content/css/font-awesome").Include("~/Content/font-awesome/css/font-awesome.css"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/ckeditor").Include("~/Scripts/ckeditor/ckeditor.js", "~/Scripts/ckeditor/adapters/jquery.js"));
		}
	}
}