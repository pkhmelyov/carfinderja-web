﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CarfinderjaWeb.Areas.Admin.Models.BlogPosts
{
    public class Create
    {
        [Required]
        public string Title { get; set; }

        [Required]
        [AllowHtml]
        [DataType(DataType.Html | DataType.MultilineText)]
        public string Body { get; set; }

        [DisplayName("Posted")]
        public bool Active { get; set; }

        public int Id { get; set; }
        public string ImageUrl { get; set; }
    }
}