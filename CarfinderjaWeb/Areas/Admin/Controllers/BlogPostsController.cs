﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using CarfinderjaWeb.Areas.Admin.Models.BlogPosts;
using CarfinderjaWeb.Data.Entities;
using CarfinderjaWeb.Data.Repositories;

namespace CarfinderjaWeb.Areas.Admin.Controllers
{
    [Authorize]
    public class BlogPostsController : Controller
    {
        private readonly int pageSize = 50;
        private readonly string uploadsVirtualPath = "~/Content/Uploads";

        private readonly IBlogPostRepository _blogPostRepository;

        public BlogPostsController(IBlogPostRepository blogPostRepository)
        {
            _blogPostRepository = blogPostRepository;
        }

        public ActionResult Index(int page = 1)
        {
            var model = _blogPostRepository.GetRecentPosts("", null, null, null, null, pageSize: pageSize, pageNumber: page);
            return View(model);
        }

        public ActionResult Details(int id)
        {
            var model = _blogPostRepository.GetById(id);
            if (model != null) return View(model);
            return HttpNotFound("There is no such Blog Post");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Create model, HttpPostedFileBase blogPostImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var post = new BlogPost
                    {
                        Title = model.Title,
                        Body = model.Body,
                        Active = model.Active,
                        CreatedOn = DateTime.UtcNow,
                        PostedOn = model.Active ? DateTime.UtcNow : (DateTime?) null,
                    };

                    UpdateImageUrl(post, blogPostImage);
                    _blogPostRepository.Add(post);
                    _blogPostRepository.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View();
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Edit(int id)
        {
            var model = _blogPostRepository.GetById(id);
            if (model != null) return View(new Create
            {
                Id = model.Id,
                Title = model.Title,
                ImageUrl = model.ImageUrl,
                Body = model.Body,
                Active = model.Active
            });
            return HttpNotFound("There is no such Blog Post");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Create model, HttpPostedFileBase blogPostImage)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var post = _blogPostRepository.GetById(model.Id);
                    if (post == null) return HttpNotFound("There is no such Blog Post");
                    post.Title = model.Title;
                    post.Body = model.Body;
                    if (!model.Active) post.PostedOn = null;
                    else if (!post.Active) post.PostedOn = DateTime.UtcNow;
                    post.Active = model.Active;
                    UpdateImageUrl(post, blogPostImage);
                    _blogPostRepository.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(model);
            }
            catch
            {
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            var post = _blogPostRepository.GetById(id);
            if (post == null) return HttpNotFound("There is no such Blog Post");
            return View(post);
        }

        [ActionName("Delete")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePost(int id)
        {
            try
            {
                var post = _blogPostRepository.GetById(id);
                if (post == null) return HttpNotFound("There is no such Blog Post");
                EnsureImageDeleted(post);
                _blogPostRepository.Delete(post);
                _blogPostRepository.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        private void EnsureUploadsFolderExists()
        {
            var uploadsPhysicalPath = Server.MapPath(uploadsVirtualPath);
            if (!Directory.Exists(uploadsPhysicalPath))
            {
                Directory.CreateDirectory(uploadsPhysicalPath);
            }
        }

        private void UpdateImageUrl(BlogPost post, HttpPostedFileBase file)
        {
            if (post != null && file != null && file.ContentLength > 0)
            {
                EnsureUploadsFolderExists();
                var extension = Path.GetExtension(file.FileName);
                var internalFileName = string.Format("{0}{1}", Guid.NewGuid().ToString("N"), extension);
                var uploadsPhysicalPath = Server.MapPath(uploadsVirtualPath);
                file.SaveAs(Path.Combine(uploadsPhysicalPath, internalFileName));
                EnsureImageDeleted(post);
                post.ImageUrl = string.Format("{0}/{1}", uploadsVirtualPath.Replace("~", ""), internalFileName);
            }
        }

        private void EnsureImageDeleted(BlogPost post)
        {
            if (!string.IsNullOrWhiteSpace(post.ImageUrl))
            {
                System.IO.File.Delete(Server.MapPath(string.Format("~{0}", post.ImageUrl)));
                post.ImageUrl = null;
            }
        }
    }
}
