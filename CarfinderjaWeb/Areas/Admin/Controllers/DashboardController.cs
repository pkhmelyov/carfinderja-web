﻿using System.Web.Mvc;
using CarfinderjaWeb.Data.Repositories;

namespace CarfinderjaWeb.Areas.Admin.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        private readonly int pageSize = 50;
        private readonly ICommentRepository _commentRepository;

        public DashboardController(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }

        public ActionResult Index(int page = 1)
        {
            var model = _commentRepository.GetLatestUnapproved(pageSize, page);
            return View(model);
        }

        public ActionResult Comment(int id)
        {
            var model = _commentRepository.GetById(id);
            if (model == null) return HttpNotFound("There is no such comment");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveComment(int id)
        {
            var comment = _commentRepository.GetById(id);
            if (comment == null) return HttpNotFound("There is no such comment");
            comment.Active = true;
            _commentRepository.SaveChanges();
            return RedirectToAction("Index", "Dashboard");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteComment(int id)
        {
            var comment = _commentRepository.GetById(id);
            if (comment == null) return HttpNotFound("There is no such comment");
            _commentRepository.Delete(comment);
            _commentRepository.SaveChanges();
            return RedirectToAction("Index", "Dashboard");
        }
    }
}