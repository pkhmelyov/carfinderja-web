﻿(function() {
	var submitButtonId = "modal_button_submit";
	var cancelButtonId = "modal_button_cancel";
	var modalId = "myModal";
	var textFieldId = "text";
	var emailFieldId = "email";
	var textGroupId = "textGroup";
	var emailGroupId = "emailGroup";
	var contactUsFormId = "contact_us_form";

	function clearForm() {
		$("#" + textFieldId).val("");
		$("#" + emailFieldId).val("");
	}

	function validateForm() {
		var valText = validateText();
		var valEmail = validateEmail();
		return valText && valEmail;
	}

	function validateText() {
		var $textGroup = $("#" + textGroupId);
		var $text = $("#" + textFieldId);
		$textGroup.removeClass("has-error");
		if ($text.val()) return true;
		$textGroup.addClass("has-error");
		return false;
	}

	function validateEmail() {
		var $emailGroup = $("#" + emailGroupId);
		var $email = $("#" + emailFieldId);
		$emailGroup.removeClass("has-error");
		var email = $email.val();
		if (email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if (re.test(email)) return true;
		}
		$emailGroup.addClass("has-error");
		return false;
	}

	function doAjax() {
		var $form = $("#" + contactUsFormId);
		var $text = $("#" + textFieldId);
		var $email = $("#" + emailFieldId);
		var $token = $form.find("input[type=hidden]:first-child");
		var data = {};
		data['text'] = $text.val();
		data['email'] = $email.val();
		data[$token.attr("name")] = $token.val();
		$.post($form.attr("action"), data, function (status) {
			if (!status.error) alert("Message has been sent");
			else alert(status.message);
		});
		clearForm();
	}

	$(function() {
		$("#" + cancelButtonId).click(function () {
			clearForm();
			$("#" + modalId).modal('hide');
		});

		$("#" + submitButtonId).click(function () {
			if (validateForm()) {
				doAjax();
				$("#" + modalId).modal('hide');
			}
		});
	});
})();