﻿namespace CarfinderjaWeb.Data
{
    public enum SortBy
    {
        PriceDesc,
        PriceAsc,
        YearDesc,
        YearAsc,
        DateAddedDesc,
        DateAddedAsc,
    }
}