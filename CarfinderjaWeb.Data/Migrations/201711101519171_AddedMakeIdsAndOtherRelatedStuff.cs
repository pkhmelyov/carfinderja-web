namespace CarfinderjaWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedMakeIdsAndOtherRelatedStuff : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Advertisements", "MakeId", c => c.Int());
            AddColumn("dbo.Advertisements", "ModelId", c => c.Int());
            AddColumn("dbo.Advertisements", "TriedToRecognizeMakeAndModel", c => c.Boolean());
            CreateIndex("dbo.Advertisements", "MakeId");
            CreateIndex("dbo.Advertisements", "ModelId");
            AddForeignKey("dbo.Advertisements", "MakeId", "dbo.Makes", "Id");
            AddForeignKey("dbo.Advertisements", "ModelId", "dbo.Models", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Advertisements", "ModelId", "dbo.Models");
            DropForeignKey("dbo.Advertisements", "MakeId", "dbo.Makes");
            DropIndex("dbo.Advertisements", new[] { "ModelId" });
            DropIndex("dbo.Advertisements", new[] { "MakeId" });
            DropColumn("dbo.Advertisements", "TriedToRecognizeMakeAndModel");
            DropColumn("dbo.Advertisements", "ModelId");
            DropColumn("dbo.Advertisements", "MakeId");
        }
    }
}
