namespace CarfinderjaWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_SiteStatistics : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SiteStatistics",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Data = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SiteStatistics");
        }
    }
}
