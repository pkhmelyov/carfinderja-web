namespace CarfinderjaWeb.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MovedAdvertismentsOut : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Advertisements", "MakeId", "dbo.Makes");
            DropForeignKey("dbo.Advertisements", "ModelId", "dbo.Models");
            DropIndex("dbo.Advertisements", new[] { "MakeId" });
            DropIndex("dbo.Advertisements", new[] { "ModelId" });
            DropTable("dbo.Advertisements");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Advertisements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WebSite = c.Int(nullable: false),
                        AddedOnUtc = c.DateTime(nullable: false),
                        Updated = c.Boolean(nullable: false),
                        Url = c.String(),
                        Title = c.String(),
                        ThumbnailUrl = c.String(),
                        Transmission = c.Int(),
                        Year = c.Int(),
                        Price = c.Decimal(precision: 18, scale: 2),
                        ConatactName = c.String(),
                        ContactPhone = c.String(),
                        ContactEmail = c.String(),
                        Location = c.String(),
                        MakeId = c.Int(),
                        ModelId = c.Int(),
                        TriedToRecognizeMakeAndModel = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Advertisements", "ModelId");
            CreateIndex("dbo.Advertisements", "MakeId");
            AddForeignKey("dbo.Advertisements", "ModelId", "dbo.Models", "Id");
            AddForeignKey("dbo.Advertisements", "MakeId", "dbo.Makes", "Id");
        }
    }
}
