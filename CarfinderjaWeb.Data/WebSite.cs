﻿namespace CarfinderjaWeb.Data
{
    public enum WebSite
    {
        JacarsNet,
        AutoadsjaCom,
        CrystalvehiclesalesCom,
        CarlandJamaicaCom,
        JetconcarsCom,
        ClajaCom,
        SuperlativeautoCom,
        WheelsandwheelsCom,
        JamaicanclassifiedsCom,
        JamaicaobserverCom,
        FiwiclassifiedsCom,
        DrivejamaicaCom,
        AutoforestjamaicaCom,
        CarsInJamaicaCom,
        JamaicaAutoTraderCom,
        JamDealCom,
    }
}