﻿using System;
using System.Collections.Generic;
using CarfinderjaWeb.Data;

namespace CarfinderjaWeb.Data
{
    public class Statistics
    {
        public int CarsForSale { get; set; }
        public int CarsAddedLastMonth { get; set; }
        public List<AdvertisedModelStats> Top10Models { get; set; }
        public List<AdvertisedBrandStats> Top10Brands { get; set; }
        public Dictionary<PriceRange, PercentRate> PriceRangeOverview { get; set; }
    }

    public class AdvertisedModelStats
    {
        public string Model { get; set; }
        public int TotalCount { get; set; }
        public double PercentRate { get; set; }
    }

    public class AdvertisedBrandStats
    {
        public string Brand { get; set; }
        public int TotalCount { get; set; }
        public double PercentRate { get; set; }
        public double RelativePercentRate { get; set; }
    }

    public class PercentRate
    {
        public double Base { get; set; }
        public double Relative { get; set; }
    }
}