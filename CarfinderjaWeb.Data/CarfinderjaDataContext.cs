﻿using CarfinderjaWeb.Data.Entities;
using CarfinderjaWeb.Data.Migrations;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CarfinderjaWeb.Data
{
    public class CarfinderjaDataContext : IdentityDbContext<AppUser>
    {
        static CarfinderjaDataContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CarfinderjaDataContext, Configuration>());
        }
        public CarfinderjaDataContext() : base("CarfinderjaDatabase") { }
        public DbSet<Make> Makes { get; set; }
        public DbSet<MakeAlias> MakeAliases { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<ModelAlias> ModelAliases { get; set; }
        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<PostComment> BlogComments { get; set; }
        public DbSet<SiteStatistics> SiteStatistics { get; set; }

        public static CarfinderjaDataContext Create()
        {
            return new CarfinderjaDataContext();
        }
    }
}
