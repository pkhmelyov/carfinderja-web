﻿namespace CarfinderjaWeb.Data
{
    public enum PriceRange
    {
        I_0_500000,
        I_500000_1000000,
        I_1000000_1500000,
        I_1500000_2000000,
        I_2000000_3000000,
        I_3000000_4000000,
        I_4000000_5000000,
        I_GT_5000000,
    }
}