﻿namespace CarfinderjaWeb.Data
{
    public enum Transmission
    {
        Automatic,
        Manual,
        Tiptronic,
        Column,
        CVT,
    }
}