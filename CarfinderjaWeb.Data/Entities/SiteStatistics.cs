﻿namespace CarfinderjaWeb.Data.Entities
{
    public class SiteStatistics
    {
        public int Id { get; set; }
        public string Data { get; set; }
    }
}
