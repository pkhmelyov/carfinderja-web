﻿using System.Collections.Generic;

namespace CarfinderjaWeb.Data.Entities
{
    public class Model
    {
        public Model()
        {
            Aliases = new List<ModelAlias>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int MakeId { get; set; }
        public virtual Make Make { get; set; }
        public virtual ICollection<ModelAlias> Aliases { get; set; }
    }
}