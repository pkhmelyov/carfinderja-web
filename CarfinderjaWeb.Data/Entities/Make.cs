﻿using System.Collections.Generic;

namespace CarfinderjaWeb.Data.Entities
{
    public class Make
    {
        public Make()
        {
            Aliases = new List<MakeAlias>();
            Models = new List<Model>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<MakeAlias> Aliases { get; set; }
        public virtual ICollection<Model> Models { get; set; }
    }
}
