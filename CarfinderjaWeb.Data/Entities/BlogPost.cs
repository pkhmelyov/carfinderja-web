﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CarfinderjaWeb.Data.Entities
{
    public class BlogPost
    {
        public BlogPost()
        {
            Comments = new List<PostComment>();
        }

        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [DataType(DataType.Html|DataType.MultilineText)]
        public string Body { get; set; }
        [DisplayName("Created On")]
        public DateTime CreatedOn { get; set; }
        [DisplayName("Posted On")]
        public DateTime? PostedOn { get; set; }
        [DisplayName("Posted")]
        public bool Active { get; set; }
        [DisplayName("Image")]
        public string ImageUrl { get; set; }

        public virtual ICollection<PostComment> Comments { get; set; }
    }
}
