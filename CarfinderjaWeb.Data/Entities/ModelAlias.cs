﻿namespace CarfinderjaWeb.Data.Entities
{
    public class ModelAlias
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public int ModelId { get; set; }
        public virtual Model Model { get; set; }
    }
}