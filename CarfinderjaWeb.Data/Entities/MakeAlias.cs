﻿namespace CarfinderjaWeb.Data.Entities
{
    public class MakeAlias
    {
        public int Id { get; set; }
        public int MakeId { get; set; }
        public string Alias { get; set; }
        public virtual Make Make { get; set; }
    }
}
