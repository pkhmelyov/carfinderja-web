﻿using System;
using System.ComponentModel;

namespace CarfinderjaWeb.Data.Entities
{
    public class PostComment
    {
        public int Id { get; set; }
        public int BlogPostId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }
        [DisplayName("Created on")]
        public DateTime CreatedOn { get; set; }
        [DisplayName("Approved")]
        public bool Active { get; set; }

        public virtual BlogPost Post { get; set; }
    }
}