﻿using System;

namespace CarfinderjaWeb.Data.Entities
{
    public class Advertisement
    {
        public int Id { get; set; }
        public WebSite WebSite { get; set; }
        public DateTime AddedOnUtc { get; set; }
        public bool Updated { get; set; }
        public string Url { get; set; }

        public string Title { get; set; }
        public string ThumbnailUrl { get; set; }
        public Transmission? Transmission { get; set; }
        public int? Year { get; set; }
        public decimal? Price { get; set; }
        public string ConatactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string Location { get; set; }
        public int? MakeId { get; set; }
        public int? ModelId { get; set; }
        public bool? TriedToRecognizeMakeAndModel { get; set; }
    }
}