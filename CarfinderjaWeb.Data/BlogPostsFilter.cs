﻿namespace CarfinderjaWeb.Data
{
    public enum BlogPostsFilter
    {
        All,
        Active,
        Inactive,
    }
}