namespace CarfinderjaWeb.Data.MigrationsAdvertisment
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Advertisements",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WebSite = c.Int(nullable: false),
                        AddedOnUtc = c.DateTime(nullable: false),
                        Updated = c.Boolean(nullable: false),
                        Url = c.String(),
                        Title = c.String(),
                        ThumbnailUrl = c.String(),
                        Transmission = c.Int(),
                        Year = c.Int(),
                        Price = c.Decimal(precision: 18, scale: 2),
                        ConatactName = c.String(),
                        ContactPhone = c.String(),
                        ContactEmail = c.String(),
                        Location = c.String(),
                        MakeId = c.Int(),
                        ModelId = c.Int(),
                        TriedToRecognizeMakeAndModel = c.Boolean(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Advertisements");
        }
    }
}
