﻿using CarfinderjaWeb.Data.Entities;
using PagedList;

namespace CarfinderjaWeb.Data.Repositories
{
    public interface ICommentRepository
    {
        PostComment GetById(int id);
        IPagedList<PostComment> GetLatestUnapproved(int pageSize, int pageNumber);
        void Delete(PostComment comment);
        void SaveChanges();
    }
}