﻿using CarfinderjaWeb.Data.Entities;
using System.Data.Entity;
using System.Linq;

namespace CarfinderjaWeb.Data.Repositories
{
    internal class MakeRepository : IMakeRepository
    {
        private readonly CarfinderjaDataContext _context;

        public MakeRepository(CarfinderjaDataContext context)
        {
            _context = context;
        }

        public void Add(string name, bool save = true)
        {
            _context.Makes.Add(new Make { Name = name });
            if (save) _context.SaveChanges();
        }

        public Make[] GetMakes()
        {
            return _context.Makes.Include(x=>x.Aliases).Include(x=>x.Models).ToArray();
        }

        public Make GetMake(string name)
        {
            name = name.ToUpperInvariant();
            var allMakes = _context.Makes.Include(x => x.Aliases).Include(x => x.Models).ToArray();
            return
                allMakes.SingleOrDefault(
                    x => x.Name.ToUpperInvariant() == name || x.Aliases.Any(y => y.Alias.ToUpperInvariant() == name));
        }

        public Model[] GetModels(int? makeId)
        {
            if (makeId == null) return Enumerable.Empty<Model>().ToArray();
            return _context.Models.Where(x => x.MakeId == makeId.Value).ToArray();
        }

        public void DeleteMake(Make make, bool save = true)
        {
            _context.Makes.Remove(make);
            if (save) _context.SaveChanges();
        }

        public void DeleteModel(Model model, bool save = true)
        {
            _context.Models.Remove(model);
            if (save) _context.SaveChanges();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}
