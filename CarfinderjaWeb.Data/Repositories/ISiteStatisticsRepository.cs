﻿using CarfinderjaWeb.Data.Entities;

namespace CarfinderjaWeb.Data.Repositories
{
    public interface ISiteStatisticsRepository
    {
        void SaveSiteStatistics(string data);
        SiteStatistics GetStatistics();
    }
}
