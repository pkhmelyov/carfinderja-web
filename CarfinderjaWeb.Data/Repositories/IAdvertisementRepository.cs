﻿using System;
using System.Threading.Tasks;
using CarfinderjaWeb.Data.Entities;
using System.Linq;
using PagedList;

namespace CarfinderjaWeb.Data.Repositories
{
    public interface IAdvertisementRepository
    {
        Advertisement GetByUrl(string url);
        IQueryable<Advertisement> GetRecent();
        int GetTotalItemsCount();
        int GetItemsCountForLastMonth(DateTime todayUtc);
        string[] GetNamesDistinct();
        void Add(Advertisement ad);
        Task SaveChangesAsync();
        Task ResetFlagAsync(WebSite site);
        Task DeleteUnchangedAsync(WebSite site);
        IPagedList<Advertisement> FillModel(int? makeId, int? modelId, Transmission? transmission, int? yearFrom,
            int? yearTo, PriceRange? priceRange, SortBy sortBy, int page, int pageSize);
        Advertisement[] GetUnprocessedAdsWithoutMakesAndModels(int count);
        Advertisement[] GetByMake(int makeId);
        Advertisement[] GetByModel(int modelId);
    }
}