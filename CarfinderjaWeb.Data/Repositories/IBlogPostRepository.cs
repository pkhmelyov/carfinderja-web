﻿using System;
using CarfinderjaWeb.Data.Entities;
using PagedList;

namespace CarfinderjaWeb.Data.Repositories
{
    public interface IBlogPostRepository
    {
        IPagedList<BlogPost> GetRecentPosts(string term, DateTime? createdFrom, DateTime? createdTo,
            DateTime? postedFrom, DateTime? postedTo, BlogPostsFilter filter = BlogPostsFilter.All, SortBy sortBy = SortBy.DateAddedDesc, int pageSize = 10,
            int pageNumber = 1);

        IPagedList<BlogPost> GetRecentActivePosts(int pageSize = 10, int pageNumber = 1);

        BlogPost GetById(int id);

        void Add(BlogPost blogPost);

        void Delete(BlogPost post);

        void SaveChanges();
    }
}