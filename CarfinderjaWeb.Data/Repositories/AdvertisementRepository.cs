﻿using CarfinderjaWeb.Data.Entities;
using PagedList;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace CarfinderjaWeb.Data.Repositories
{
    internal class AdvertisementRepository : IAdvertisementRepository
    {
        private readonly CarfinderjaAdvertismentDataContext _context;

        public AdvertisementRepository(CarfinderjaAdvertismentDataContext context)
        {
            _context = context;
        }

        public Advertisement GetByUrl(string url)
        {
            return _context.Advertisements.SingleOrDefault(x => x.Url == url);
        }

        public IQueryable<Advertisement> GetRecent()
        {
            return _context.Advertisements.OrderByDescending(x => x.AddedOnUtc);
        }

        public int GetTotalItemsCount()
        {
            return _context.Advertisements.Count();
        }

        public int GetItemsCountForLastMonth(DateTime todayUtc)
        {
            DateTime edge = todayUtc.AddDays(-30);
            return _context.Advertisements.Count(x => x.AddedOnUtc <= todayUtc && x.AddedOnUtc >= edge);
        }

        public string[] GetNamesDistinct()
        {
            return _context.Advertisements.Select(x => x.Title).Distinct().ToArray();
        }

        public void Add(Advertisement ad)
        {
            _context.Advertisements.Add(ad);
        }

        public Task SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }

        public Task ResetFlagAsync(WebSite site)
        {
            return _context.Advertisements.Where(x => x.WebSite == site).ForEachAsync(x => x.Updated = false);
        }

        public Task DeleteUnchangedAsync(WebSite site)
        {
            return _context.Advertisements.Where(x => x.WebSite == site && !x.Updated)
                .ForEachAsync(x => _context.Entry(x).State = EntityState.Deleted);
        }

        public IPagedList<Advertisement> FillModel(int? makeId, int? modelId, Transmission? transmission, int? yearFrom, int? yearTo, PriceRange? priceRange, SortBy sortBy, int page, int pageSize)
        {
            var query = _context.Advertisements.Where(x =>
                (makeId == null || x.MakeId == makeId)
                && (modelId == null || x.ModelId == modelId)
                && (transmission == null || x.Transmission == transmission)
                && (yearFrom == null || x.Year >= yearFrom)
                && (yearTo == null || x.Year <= yearTo));
            switch (priceRange)
            {
                case PriceRange.I_0_500000:
                    query = query.Where(x => x.Price <= 500000);
                    break;
                case PriceRange.I_500000_1000000:
                    query = query.Where(x => x.Price > 500000 && x.Price <= 1000000);
                    break;
                case PriceRange.I_1000000_1500000:
                    query = query.Where(x => x.Price > 1000000 && x.Price <= 1500000);
                    break;
                case PriceRange.I_1500000_2000000:
                    query = query.Where(x => x.Price > 1500000 && x.Price <= 2000000);
                    break;
                case PriceRange.I_2000000_3000000:
                    query = query.Where(x => x.Price > 2000000 && x.Price <= 3000000);
                    break;
                case PriceRange.I_3000000_4000000:
                    query = query.Where(x => x.Price > 3000000 && x.Price <= 4000000);
                    break;
                case PriceRange.I_4000000_5000000:
                    query = query.Where(x => x.Price > 4000000 && x.Price <= 5000000);
                    break;
                case PriceRange.I_GT_5000000:
                    query = query.Where(x => x.Price > 5000000);
                    break;
            }
            switch (sortBy)
            {
                case SortBy.PriceDesc:
                    query = query.OrderByDescending(item => item.Price);
                    break;
                case SortBy.PriceAsc:
                    query = query.OrderBy(item => item.Price);
                    break;
                case SortBy.YearDesc:
                    query = query.OrderByDescending(item => item.Year);
                    break;
                case SortBy.YearAsc:
                    query = query.OrderBy(item => item.Year);
                    break;
                case SortBy.DateAddedDesc:
                    query = query.OrderByDescending(x => x.AddedOnUtc);
                    break;
                case SortBy.DateAddedAsc:
                    query = query.OrderBy(item => item.AddedOnUtc);
                    break;
            }
            return query.ToPagedList(page, pageSize);
        }

        public Advertisement[] GetUnprocessedAdsWithoutMakesAndModels(int count)
        {
            return _context.Advertisements.Where(x => x.TriedToRecognizeMakeAndModel != true).OrderBy(x => x.Id).Take(count).ToArray();
        }

        public Advertisement[] GetByMake(int makeId)
        {
            return _context.Advertisements.Where(x => x.MakeId == makeId).ToArray();
        }

        public Advertisement[] GetByModel(int modelId)
        {
            return _context.Advertisements.Where(x => x.ModelId == modelId).ToArray();
        }
    }
}