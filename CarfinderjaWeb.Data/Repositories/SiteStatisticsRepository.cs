﻿using CarfinderjaWeb.Data.Entities;
using System.Linq;

namespace CarfinderjaWeb.Data.Repositories
{
    public class SiteStatisticsRepository : ISiteStatisticsRepository
    {
        private readonly CarfinderjaDataContext _context;

        public SiteStatisticsRepository(CarfinderjaDataContext context)
        {
            _context = context;
        }

        public SiteStatistics GetStatistics()
        {
            return _context.SiteStatistics.OrderByDescending(x => x.Id).FirstOrDefault();
        }

        public void SaveSiteStatistics(string data)
        {
            _context.SiteStatistics.RemoveRange(_context.SiteStatistics.ToArray());
            _context.SiteStatistics.Add(new SiteStatistics { Data = data });
            _context.SaveChanges();
        }
    }
}
