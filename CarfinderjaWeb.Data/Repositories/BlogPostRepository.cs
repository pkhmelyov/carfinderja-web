﻿using System;
using System.Data.Entity;
using System.Linq;
using CarfinderjaWeb.Data.Entities;
using PagedList;

namespace CarfinderjaWeb.Data.Repositories
{
    internal class BlogPostRepository : IBlogPostRepository
    {
        private readonly CarfinderjaDataContext _context;

        public BlogPostRepository(CarfinderjaDataContext context)
        {
            _context = context;
        }

        public IPagedList<BlogPost> GetRecentPosts(string term, DateTime? createdFrom, DateTime? createdTo, DateTime? postedFrom, DateTime? postedTo, BlogPostsFilter filter = BlogPostsFilter.All, SortBy sortBy = SortBy.DateAddedDesc, int pageSize = 10, int pageNumber = 1)
        {
            IQueryable<BlogPost> query = _context.BlogPosts;
            switch (sortBy)
            {
                case SortBy.DateAddedAsc:
                    query = query.OrderBy(x => x.CreatedOn);
                    break;
                default:
                    query = query.OrderByDescending(x => x.CreatedOn);
                    break;
            }
            return query.ToPagedList(pageNumber, pageSize);
        }

        public IPagedList<BlogPost> GetRecentActivePosts(int pageSize = 10, int pageNumber = 1)
        {
            return _context.BlogPosts.Where(x => x.Active)
                .OrderByDescending(x => x.PostedOn)
                .ToPagedList(pageNumber, pageSize);
        }

        public BlogPost GetById(int id)
        {
            return _context.BlogPosts.Find(id);
        }

        public void Add(BlogPost blogPost)
        {
            _context.BlogPosts.Add(blogPost);
        }

        public void Delete(BlogPost post)
        {
            _context.Entry(post).State = EntityState.Deleted;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }
    }
}