﻿using System;
using System.Data.Entity;
using System.Linq;
using CarfinderjaWeb.Data.Entities;
using PagedList;

namespace CarfinderjaWeb.Data.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        private readonly CarfinderjaDataContext _context;

        public CommentRepository(CarfinderjaDataContext context)
        {
            _context = context;
        }

        public PostComment GetById(int id)
        {
            return _context.BlogComments.Find(id);
        }

        public void Delete(PostComment comment)
        {
            _context.Entry(comment).State = EntityState.Deleted;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public IPagedList<PostComment> GetLatestUnapproved(int pageSize, int pageNumber)
        {
            return _context.BlogComments.Where(x => !x.Active)
                .OrderBy(x => x.CreatedOn)
                .ToPagedList(pageNumber, pageSize);
        }
    }
}