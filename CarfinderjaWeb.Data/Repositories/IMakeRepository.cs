﻿using CarfinderjaWeb.Data.Entities;

namespace CarfinderjaWeb.Data.Repositories
{
    public interface IMakeRepository
    {
        void Add(string name, bool save = true);
        Make[] GetMakes();
        Make GetMake(string name);
        Model[] GetModels(int? makeId);
        void DeleteMake(Make make, bool save = true);
        void DeleteModel(Model model, bool save = true);
        void SaveChanges();
    }
}