﻿using CarfinderjaWeb.Data.Entities;
using CarfinderjaWeb.Data.MigrationsAdvertisment;
using System.Data.Entity;

namespace CarfinderjaWeb.Data
{
    public class CarfinderjaAdvertismentDataContext : DbContext
    {
        static CarfinderjaAdvertismentDataContext()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<CarfinderjaAdvertismentDataContext, Configuration>());
        }
        public CarfinderjaAdvertismentDataContext() : base("CarfinderjaAdvertismentsDatabase") { }
        public DbSet<Advertisement> Advertisements { get; set; }
    }
}
