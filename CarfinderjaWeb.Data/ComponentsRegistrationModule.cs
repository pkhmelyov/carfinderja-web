﻿using Autofac;
using CarfinderjaWeb.Data.Repositories;

namespace CarfinderjaWeb.Data
{
    public class ComponentsRegistrationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CarfinderjaDataContext>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<CarfinderjaAdvertismentDataContext>().AsSelf().InstancePerLifetimeScope();
            builder.RegisterType<AdvertisementRepository>().As<IAdvertisementRepository>().InstancePerDependency();
            builder.RegisterType<MakeRepository>().As<IMakeRepository>().InstancePerDependency();
            builder.RegisterType<BlogPostRepository>().As<IBlogPostRepository>().InstancePerDependency();
            builder.RegisterType<CommentRepository>().As<ICommentRepository>().InstancePerDependency();
            builder.RegisterType<SiteStatisticsRepository>().As<ISiteStatisticsRepository>().InstancePerDependency();
        }
    }
}